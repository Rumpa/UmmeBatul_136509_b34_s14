<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../assest/bootstrap/css/bootstrap.min.css">
    <script src="../../assest/bootstrap/js/jquery.min.js"></script>
    <script src="../../assest/bootstrap/js/bootstrap.min.js"></script>
</head>
<body style="background-color: lightblue">

<div class="container">
    <h2>Book Title</h2>
    <form>
        <div class="form-group">
            <label for="booktitle">Book Title:</label>
            <input type="text" style="background-color:olive;color: white"  class="form-control" id="booktitle" placeholder="Enter Book Title">
        </div>
        <div class="form-group">
            <label for="authorname">Password:</label>
            <input type="text" style="background-color: olive;color: white" class="form-control" id="authorname" placeholder="Enter Author Name">
        </div>

        <button type="submit" class="btn-lg btn-primary ">Add</button>
    </form>
</div>

</body>
</html>

